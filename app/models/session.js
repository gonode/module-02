module.exports = (sequelize, DataTypes) =>
    sequelize.define('Session', {
        sid: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        expires: DataTypes.DATE,
        data: DataTypes.TEXT,
    });
