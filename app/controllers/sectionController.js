const { Project, Section } = require('../models');

module.exports = {
    async create(req, res, next) {
        try {
            const { id } = req.params;

            const project = await Project.findById(id);
            const sections = await Section.findAll({ where: { ProjectId: id } });

            return res.render('section/create', { project, sections });
        } catch (err) {
            return next(err);
        }
    },

    async view(req, res, next) {
        try {
            const { projectId, sectionId } = req.params;

            const project = await Project.findById(projectId);
            const sections = await Section.findAll({ where: { ProjectId: projectId } });
            const activeSection = await Section.findById(sectionId);

            return res.render('section/view', {
                project,
                sections,
                activeSection,
            });
        } catch (err) {
            return next(err);
        }
    },

    async save(req, res, next) {
        try {
            const sectionContent = req.body.content.trim();

            if (sectionContent) {
                const { id: ProjectId } = req.params;

                await Section.create({
                    ...req.body,
                    ProjectId,
                });

                req.flash('success', 'Seção criada com sucesso!');

                return res.redirect(`/app/project/${ProjectId}`);
            }

            req.flash('error', 'O conteúdo da seção não pode ser vazio.');

            return res.redirect('back');
        } catch (err) {
            return next(err);
        }
    },

    async update(req, res, next) {
        try {
            const sectionContent = req.body.content.trim();

            if (sectionContent) {
                const { projectId, sectionId } = req.params;

                const section = await Section.findById(sectionId);

                await section.update(req.body);

                req.flash('success', 'Seção atualizada com sucesso!');

                return res.redirect(`/app/project/${projectId}/section/${sectionId}`);
            }

            req.flash('error', 'O conteúdo da seção não pode ser vazio.');

            return res.redirect('back');
        } catch (err) {
            return next(err);
        }
    },

    async destroy(req, res, next) {
        try {
            const { projectId, sectionId } = req.params;

            await Section.destroy({ where: { id: sectionId } });

            req.flash('success', 'Seção removida com sucesso!');

            return res.redirect(`/app/project/${projectId}`);
        } catch (err) {
            return next(err);
        }
    },
};
