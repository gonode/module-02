const { Project, Section } = require('../models');

module.exports = {
    async view(req, res, next) {
        try {
            const project = await Project.findById(req.params.id);

            const sections = await Section.findAll({
                where: {
                    ProjectId: req.params.id,
                },
            });

            return res.render('project/view', {
                project,
                sections,
            });
        } catch (err) {
            return next(err);
        }
    },

    async save(req, res, next) {
        try {
            await Project.create({
                ...req.body,
                UserId: req.session.user.id,
            });

            req.flash('success', 'Projeto criado com sucesso!');

            return res.redirect('back');
        } catch (err) {
            return next(err);
        }
    },

    async destroy(req, res, next) {
        try {
            await Project.destroy({ where: { id: req.params.id } });

            req.flash('success', 'Projeto removido com sucesso!');

            return res.redirect('/app/dashboard');
        } catch (err) {
            return next(err);
        }
    },
};
