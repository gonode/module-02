
module.exports = (req, res, next) => {
    if (req.session && req.session.user) {
        return next();
    }

    req.flash('error', 'Você precisa fazer login para acessar esta página!');
    return res.redirect('/');
};
