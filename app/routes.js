const express = require('express');

const routes = express.Router();

const authMiddleware = require('./middlewares/auth');
const guestMiddleware = require('./middlewares/guest');
const authController = require('./controllers/authController');
const dashboardController = require('./controllers/dashboardController');
const projectController = require('./controllers/projectController');
const sectionController = require('./controllers/sectionController');

routes.use((req, res, next) => {
    res.locals.flashSuccess = req.flash('success');
    res.locals.flashError = req.flash('error');
    res.locals.username = req.session.user ? req.session.user.name : '';
    res.locals.baseUrl = req.headers.host;
    next();
});

/*
* Auth
* */
routes.get('/', guestMiddleware, authController.signin);
routes.get('/signup', guestMiddleware, authController.signup);
routes.get('/signout', authController.signout);
routes.post('/register', authController.register);
routes.post('/authenticate', authController.authenticate);

/*
* Dashboard
* */
routes.use('/app', authMiddleware);
routes.get('/app/dashboard', dashboardController.index);

/*
* Project
* */
routes.get('/app/project/:id', projectController.view);
routes.post('/app/project/create', projectController.save);
routes.delete('/app/project/:id', projectController.destroy);

/*
* Section
* */
routes.post('/app/project/:id/section/save', sectionController.save);
routes.get('/app/project/:id/section/create', sectionController.create);
routes.get('/app/project/:projectId/section/:sectionId', sectionController.view);
routes.put('/app/project/:projectId/section/:sectionId', sectionController.update);
routes.delete('/app/project/:projectId/section/:sectionId', sectionController.destroy);

/*
* Error
* */
routes.use((req, res) => res.render('errors/404'));

routes.use((err, req, res, _next) => {
    res.status(err.status || 500);

    return res.render('errors/index', {
        message: err.message,
        error: process.env.NODE_ENV === 'production' ? {} : err,
    });
});

module.exports = routes;
